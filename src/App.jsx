import { useState, useEffect } from 'react'
import './App.css'

function checkRows(matrix) {
  const n = matrix.length;
  for (let i = 0; i < n; i++) {
    if ((matrix[i].includes("X") && !matrix[i].includes("O") && !matrix[i].includes(null)) || (matrix[i].includes("O") && !matrix[i].includes("X") && !matrix[i].includes(null))) {
      return true;
    }
  }
  return false;
}
function getInverseMatrix(matrix) {
  const n = matrix.length;
  const inverseMatrix = Array(n).fill(0).map(() => Array(n).fill(0));
  for (let i = 0; i < n; i++) {
    for (let j = 0; j < n; j++) {
      inverseMatrix[j][i] = matrix[i][j] || null;
    }
  }
  return inverseMatrix
}

function checkColumns(matrix) {
  const inverseMatrix = getInverseMatrix(matrix);
  return checkRows(inverseMatrix);
}

function checkDiagonals(matrix) {
  const n = matrix.length;
  const firstDiagonal = Array(n).fill(0);
  const secondDiagonal = Array(n).fill(0);
  for (let i = 0; i < n; i++) {
    firstDiagonal[i] = matrix[i][i];
    secondDiagonal[i] = matrix[i][n - i - 1];
  }
 if((firstDiagonal.includes("X") && !firstDiagonal.includes("O") && !firstDiagonal.includes(null)) || (firstDiagonal.includes("O") && !firstDiagonal.includes("X") && !firstDiagonal.includes(null))){
  return true;
 } else if((secondDiagonal.includes("X") && !secondDiagonal.includes("O") && !secondDiagonal.includes(null)) || (secondDiagonal.includes("O") && !secondDiagonal.includes("X") && !secondDiagonal.includes(null))){
  return true
 }

 return false;
  
}


function calculateWinner(arrayMatrix) {
  // There are certain possible ways to win n rows, n columns and 2 diagonals
  // 1. Check rows
  const rows = checkRows(arrayMatrix);
  if (rows) {
    return true;
  }
  // 2. Check columns
  const columns = checkColumns(arrayMatrix);
  if (columns) {
    return true;
  }
  // 3. Check diagonals
  const diagonals = checkDiagonals(arrayMatrix);
  if (diagonals) {
    return true;
  }
  return false;
}

function createMatrix(n) {
  const matrix = [];
  for (let i = 0; i < n; i++) {
    matrix.push(Array(n).fill(null));
  }
  return matrix;
}

function modifyMatrix(matrix, row, col, value) {
  const newMatrix = matrix.map((row) => [...row]);
  newMatrix[row][col] = value === 1 ? "X" : "O";
  return newMatrix;
}

function nextPlayer(previousPlayer) {
  return previousPlayer === 1 ? 2 : 1;
}

function App() {
  const [matrix, setMatrix] = useState(createMatrix(3));
  const [player, setPlayer] = useState(1);
  const [winner, setWinner] = useState(null);

  useEffect(() => {
    const gameWon = calculateWinner(matrix);
    if (gameWon) {
      setWinner(() => player === 1 ? 2 : 1);
    }
  }, [matrix]);

  function renderMatrix(matrix) {
    return matrix.map((row, rowIndex) => (
      <div key={rowIndex} className="row">
        {row.map((col, colIndex) => (
          <div key={colIndex} className="col" onClick={() => { setMatrix((matrix) => modifyMatrix(matrix, rowIndex, colIndex, player)); setPlayer(previousPlayer => nextPlayer(previousPlayer)) }}>
            {col}
          </div>
        ))}
      </div>
    ));
  }

  function resetGame() {
    setMatrix(createMatrix(3));
    setPlayer(1);
    setWinner(null);
  }

  function renderWinner() {
    return (
      <>
        <h2>Winner is player {winner}</h2>
        <button onClick={resetGame}>Reset Game</button>
      </>
    )
  }

  return (
    <>
      <h1>Game of tic tac toe</h1>
      <div className="card">
        {(winner === null) ? renderMatrix(matrix) : renderWinner()}
        <div className='bottom-reset'>
        {(winner === null) && <button onClick={resetGame}>Reset Game</button>}
        </div>
      </div>
    </>
  )
}

export default App
